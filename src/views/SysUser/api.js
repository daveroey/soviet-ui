const apiPrefix = '/sys-user';
import { defHttp } from '/@/utils/http/axios';

export function GetList(query) {
  return defHttp.post({
    url: apiPrefix + '/page',
    data: query,
  });
}

export function AddObj(obj) {
  return request({
    url: apiPrefix + '/add',
    method: 'post',
    data: obj,
  });
}

export function UpdateObj(obj) {
  return request({
    url: apiPrefix + '/update',
    method: 'post',
    data: obj,
  });
}

export function DelObj(id) {
  return request({
    url: apiPrefix + '/delete',
    method: 'post',
    params: { id },
  });
}

export function GetObj(id) {
  return request({
    url: apiPrefix + '/info',
    method: 'get',
    params: { id },
  });
}
