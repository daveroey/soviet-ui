import * as api from './api';
import { dict } from '@fast-crud/fast-crud';
// 构建crudOptions的方法
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const editRequest = async ({ form, row }) => {
    form.id = row.id;
    return await api.UpdateObj(form);
  };
  const delRequest = async (id) => {
    return await api.DelObj(id);
  };

  const addRequest = async ({ form }) => {
    return await api.AddObj(form);
  };
  return {
    crudOptions: {
      //请求配置
      request: {
        pageRequest, // 列表数据请求
        addRequest, // 添加请求
        editRequest, // 修改请求
        delRequest, // 删除请求
      },
      columns: {
        id: {
          title: 'ID',
          type: 'text',
        },
        username: {
          title: '用户名',
          type: 'text',
        },
        password: {
          title: '密码',
          type: 'text',
        },
        phone: {
          title: '电话',
          type: 'text',
        },
        createTime: {
          title: '创建时间',
          type: 'text',
        },
        updateTime: {
          title: '更新时间',
          type: 'text',
        },
        state: {
          title: '用户状态',
          type: 'text',
        },
        isDel: {
          title: '是否删除',
          type: 'text',
        },
      },
      // 其他crud配置
    },
  };
}
