import { defHttp } from '/@/utils/http/axios';

enum Api {
  AddUser = '/system/user/add',
  DeleteUser = '/system/user/delete',
  UpdateUser = '/system/user/update',
}

export const AddUser = (params) => defHttp.post({ url: Api.AddUser, params });

export const deleteUser = (id?: number) => defHttp.get({ url: Api.DeleteUser, params: { id } });

export const UpdateUser = (params) => defHttp.post({ url: Api.UpdateUser, params });
