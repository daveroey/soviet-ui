import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const sys: AppRouteModule = {
  path: '/sys',
  name: 'Sys',
  component: LAYOUT,
  redirect: '/sys/user',
  meta: {
    icon: 'ion:grid-outline',
    title: 'gg',
  },
  children: [
    {
      path: 'user',
      name: 'User',
      component: () => import('/@/views/SysUser/index.vue'),
      meta: {
        affix: true,
        title: 'gg1',
      },
    },
  ],
};
export default sys;
